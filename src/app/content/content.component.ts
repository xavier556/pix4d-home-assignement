import { Component, OnInit } from '@angular/core';
import { ContentService } from '../content.service';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {

  private entries;
  private assets;

  constructor(private contentService: ContentService) { }

  ngOnInit() {
    this.contentService.getEntries().subscribe(data => {
      this.entries = data;
    });
  }

}
