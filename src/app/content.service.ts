import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const BASE_URL = 'https://cdn.contentful.com';
const IMAGE_BASE_URL = 'https://images.ctfassets.net'
const SPACE = 'e496ylt022o4';
const ACCESS_TOKEN = 'aztNkQIAvQghiWpnA7cjhFYf-Fg34B_lo2F2lY-DneM';

@Injectable({
  providedIn: 'root'
})
export class ContentService {

  constructor(private http: HttpClient) { }

  /**
   * Call the api and return an observable array of objects to use in components.
   */
  getEntries(): Observable<Object[]> {

    // url to get entries as specified in the doc
    let url: string = BASE_URL + '/spaces/' + SPACE + '/entries?access_token=' + ACCESS_TOKEN;
    
    return this.http.get(url).pipe(
      map(res => {

        // entries are in the items property of the response
        return res['items']

          // compared to the given design, items are returned in reverse order
          .reverse()

          // map to a small object with just the properties we need
          .map(item => {
            return {
              title: item.fields.title,
              content: item.fields.content,

              // @todo use the Image API to do this properly
              image: res['includes'].Asset.filter(a => a.sys.id === item.fields.image.sys.id).map(a => {
                return {
                  url: a.fields.file.url,
                  alt: a.fields.title
                }
              })[0]
            }
          })
      })
    );
  }
}
